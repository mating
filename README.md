# mating

compute and visualize slow mating of quadratic polynomial Julia sets

## about

see blog post:

<https://mathr.co.uk/blog/2020-01-16_slow_mating_of_quadratic_julia_sets.html>

## usage

for a single image:

    make
    ./mating -- -1 0 -0.122 0.75 > basilica-vs-rabbit.ppm

for videos pipe the output to ffmpeg:

    make
    ./mating --video -- -1 0 -0.122 0.75 |
    ffmpeg -f image2pipe -i - \
      -pix_fmt yuv420p -profile:v high -level:v 4.1 -crf:v 20 \
      -movflags +faststart \
      basilica-vs-rabbit.mp4

for higher resolution or longer videos, try --help for instructions

## future

- OpenCL for GPU image rendering
- handle (pre-)periodic orbits specially for speed boost
- use period information for colouring
- make quality presets runtime selectable

## legal

mating -- slow mating of quadratic polynomial Julia sets
Copyright (C) 2020  Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


-- 
<https://mathr.co.uk>
