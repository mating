/*
mating (c) 2020 Claude Heiland-Allen
License: GNU AGPL3+
*/

// for getopt_long()
#define _GNU_SOURCE

#include <complex.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <getopt.h>

static const double pi = 3.141592653589793;

// avoid unnecessary square roots
static inline double cnorm(double _Complex z)
{
  double x = creal(z);
  double y = cimag(z);
  return x * x + y * y;
}

// dual-complex numbers for automatic differentiation

typedef struct
{
  double _Complex z, dz;
} dcomplex;

static inline dcomplex dc_const(double _Complex a)
{
  dcomplex c = { a, 0 };
  return c;
}

static inline dcomplex dc_proj(dcomplex a)
{
  dcomplex c = { cproj(a.z), cproj(a.dz) };
  return c;
}

static inline dcomplex dc_add(dcomplex a, dcomplex b)
{
  dcomplex c = { a.z + b.z, a.dz + b.dz };
  return c;
}

static inline dcomplex dc_addc(dcomplex a, double _Complex b)
{
  dcomplex c = { a.z + b, a.dz };
  return c;
}

static inline dcomplex dc_sub(dcomplex a, dcomplex b)
{
  dcomplex c = { a.z - b.z, a.dz - b.dz };
  return c;
}

static inline dcomplex dc_sqr(dcomplex a)
{
  dcomplex c = { a.z * a.z, 2 * a.z * a.dz };
  return c;
}

static inline dcomplex dc_cmul(double _Complex a, dcomplex b)
{
  dcomplex c = { a * b.z, a * b.dz };
  return c;
}

static inline dcomplex dc_mul(dcomplex a, dcomplex b)
{
  dcomplex c = { a.z * b.z, a.z * b.dz + a.dz * b.z };
  return c;
}

static inline dcomplex dc_div(dcomplex a, dcomplex b)
{
  dcomplex c = { a.z / b.z, (a.dz * b.z - a.z * b.dz) / (b.z * b.z) };
  return c;
}

static inline dcomplex dc_cdiv(double _Complex a, dcomplex b)
{
  dcomplex c = { a / b.z, (- a * b.dz) / (b.z * b.z) };
  return c;
}

// 3-vectors for RGB colours

typedef struct
{
  double v[3];
} vec3;

static inline vec3 v3_add(vec3 a, vec3 b)
{
  vec3 c = { { a.v[0] + b.v[0], a.v[1] + b.v[1], a.v[2] + b.v[2] } };
  return c;
}

static inline vec3 v3_adds(vec3 a, double b)
{
  vec3 c = { { a.v[0] + b, a.v[1] + b, a.v[2] + b } };
  return c;
}

static inline vec3 v3_sadd(double b, vec3 a)
{
  return v3_adds(a, b);
}

static inline vec3 v3_sub(vec3 a, vec3 b)
{
  vec3 c = { { a.v[0] - b.v[0], a.v[1] - b.v[1], a.v[2] - b.v[2] } };
  return c;
}

static inline vec3 v3_subs(vec3 a, double b)
{
  vec3 c = { { a.v[0] - b, a.v[1] - b, a.v[2] - b } };
  return c;
}

static inline vec3 v3_mul(vec3 a, vec3 b)
{
  vec3 c = { { a.v[0] * b.v[0], a.v[1] * b.v[1], a.v[2] * b.v[2] } };
  return c;
}

static inline vec3 v3_smul(double b, vec3 a)
{
  vec3 c = { { a.v[0] * b, a.v[1] * b, a.v[2] * b } };
  return c;
}

static inline vec3 v3_muls(vec3 a, double b)
{
  return v3_smul(b, a);
}

static inline vec3 v3_mins(vec3 a, double b)
{
  vec3 c = { { fmin(a.v[0], b), fmin(a.v[1], b), fmin(a.v[2], b) } };
  return c;
}

static inline vec3 v3_maxs(vec3 a, double b)
{
  vec3 c = { { fmax(a.v[0], b), fmax(a.v[1], b), fmax(a.v[2], b) } };
  return c;
}

static inline vec3 v3_clamp(vec3 a, double lo, double hi)
{
  return v3_mins(v3_maxs(a, lo), hi);
}

static inline vec3 v3_mix(vec3 a, vec3 b, double x)
{
  return v3_add(a, v3_muls(v3_sub(b, a), x));
}

static inline vec3 v3_abs(vec3 a)
{
  vec3 c = { { fabs(a.v[0]), fabs(a.v[1]), fabs(a.v[2]) } };
  return c;
}

static inline vec3 v3_floor(vec3 a)
{
  vec3 c = { { floor(a.v[0]), floor(a.v[1]), floor(a.v[2]) } };
  return c;
}

static inline vec3 v3_fract(vec3 a)
{
  return v3_sub(a, v3_floor(a));
}

static inline vec3 v3_smix(double a, vec3 b, double x)
{
  return v3_sadd(a, v3_muls(v3_subs(b, a), x));
}

// based on <http://lolengine.net/blog/2013/07/27/rgb-to-hsv-in-glsl>
static inline vec3 hsv2rgb(vec3 c)
{
  vec3 K = { { 1.0, 2.0 / 3.0, 1.0 / 3.0 } };
  vec3 p = v3_abs(v3_subs(v3_muls(v3_fract(v3_sadd(c.v[0], K)), 6), 3));
  p = v3_clamp(v3_subs(p, K.v[0]), 0.0, 1.0);
  return v3_smul(c.v[2], v3_smix(1.0, p, c.v[1]));
}

static inline double linear2srgb(double c)
{
  c = fmin(fmax(c, 0), 1);
  const double a = 0.055;
  if (c <= 0.0031308)
    return 12.92 * c;
  else
    return (1.0 + a) * pow(c, 1.0 / 2.4) - a;
}

static const vec3 black = { { 0, 0, 0 } };
static const vec3 grey  = { { 0.5, 0.5, 0.5 } };
static const vec3 white = { { 1, 1, 1 } };

// deterministic pseudo-random number generator

static inline uint32_t hash(uint32_t a)
{
  a = (a+0x7ed55d16u) + (a<<12u);
  a = (a^0xc761c23cu) ^ (a>>19u);
  a = (a+0x165667b1u) + (a<<5u);
  a = (a+0xd3a2646cu) ^ (a<<9u);
  a = (a+0xfd7046c5u) + (a<<3u);
  a = (a^0xb55a4f09u) ^ (a>>16u);
  return a;
}

static inline double uniform
  (uint32_t a, uint32_t b, uint32_t c, uint32_t d)
{
  return hash(a + hash(b + hash(c + hash(d))))
    / (double) (1ULL << 32ULL);
}

// program entry point
extern int main(int argc, char **argv)
{

  // run time configuration
  int quality = 0; // 0 = ld, sd, hd, 4k, 8k = 4
  int single_image_only = 1;
  int single_image_s = 0;
  int single_image_n = 12;
  double _Complex p, q;

  // animation parameters
  int segment_steps = 16;
  int iterations = 25;

  // Julia set quality
  int julia_iterations = 1000;

  // parse arguments
  while (1)
  {
    int option_index = 0;
    static struct option long_options[] =
      { {"quality", required_argument, 0,  0 }
      , {"video",   no_argument,       0,  0 }
      , {"help",    no_argument,       0, 'h'}
      , {0,         0,                 0,  0 }
      };
    int c = getopt_long(argc, argv, "S:s:N:n:h?", long_options, &option_index);
    if (c == -1)
    {
      break;
    }
    switch (c)
    {
      case 0:
        switch (option_index)
        {
          case 0: // quality
            quality = atoi(optarg);
            if (! (0 <= quality && quality <= 4))
            {
              fprintf(stderr, "quality out of range\n");
              return 1;
            }
            break;
          case 1: // video
            single_image_only = 0;
            break;
        }
        break;
      case 'S':
        segment_steps = atoi(optarg);
        break;
      case 'N':
        iterations = atoi(optarg);
        break;
      case 's':
        single_image_s = atoi(optarg);
        break;
      case 'n':
        single_image_n = atoi(optarg);
        break;
      case 'J':
        julia_iterations = atoi(optarg);
        break;
      case '?':
      case 'h':
        fprintf
          ( stderr
          , "usage: %s [options] -- pre pim qre qim > out.ppm\n"
            "  pre + pim i and qre + qim i must be in M\n"
            "  (and not in conjuate limbs)\n"
            "options:\n"
            "  --quality q      select quality in 0,1,2,3,4\n"
            "  -J j             set Julia set iterations\n"
            "  -s s, -n n       select frame for single image\n"
            "  -S s, -N n       set animation parameters\n"
            "  --video          render image sequence\n"
            "  --help, -h, -?   show this message\n"
          , argv[0]
          );
       return 0;
    }
  }
  if (optind + 4 == argc)
  {
    p = atof(argv[optind + 0]) + I * atof(argv[optind + 1]);
    q = atof(argv[optind + 2]) + I * atof(argv[optind + 3]);
  }
  else
  {
    fprintf(stderr, "bad arguments, try --help\n");
    return 1;
  }

  // quality presets
  const int width = 1 << (quality + 9);
  const int height = 1 << (quality + 8);
  const int min_samples = 4 << (quality >> 1);
  const int max_samples = 4 << (quality << 1);

  // output image buffer
  size_t ppm_bytes = (size_t) height * width * 3;
  unsigned char *ppm = malloc(ppm_bytes);
  if (! ppm)
  {
    return 1;
  }

  // TODO: handle exactly (pre-)periodic orbits specially (faster)
  int p_length = iterations;
  int q_length = iterations;

  // initialize radius
  double R[segment_steps];
  double R1 = 1e10;
  double R2 = R1 * R1;
  double R4 = R2 * R2;
  for (int s = 0; s < segment_steps; ++s)
  {
    double t = (s + 0.5) / segment_steps;
    R[s] = exp(pow(2, 1 - t) * log(R1));
  }

  // initialize orbits
  double _Complex p_0[p_length];
  {
    double _Complex z = 0;
    for (int n = 0; n < p_length; ++n)
    {
      p_0[n] = z;
      z = z * z + p;
    }
  }
  double _Complex q_0[q_length];
  {
    double _Complex z = 0;
    for (int n = 0; n < q_length; ++n)
    {
      q_0[n] = z;
      z = z * z + q;
    }
  }

  // initialize path segments
  double _Complex x0[p_length][segment_steps];
  double _Complex y0[q_length][segment_steps];
  for (int s = 0; s < segment_steps; ++s)
  {
    double t = (s + 0.5) / segment_steps;
    for (int n = 0; n < p_length; ++n)
    {
      // use cproj to normalize representation of infinity
      x0[n][s] = cproj
              ( (1 + (1 - t) * q / R2)
              / (1 + (1 - t) * p / R2)
              * (p_0[n] / R[s])
              / (1 + (1 - t) * q / R4 * (p_0[n] - p))
              );
    }
    for (int n = 0; n < q_length; ++n)
    {
      // use cproj to normalize representation of infinity
      y0[n][s] = cproj
              ( (1 + (1 - t) * q / R2)
              / (1 + (1 - t) * p / R2)
              * (R[s] / q_0[n])
              * (1 + (1 - t) * p / R4 * (q_0[n] - q))
              );
    }
  }

  // fixed points (for colouring)
  double _Complex c0_p = (1 - csqrt(1 - 4 * p)) / 2;
  double _Complex c0_q = (1 - csqrt(1 - 4 * q)) / 2;

  // store inverse of pullback function
  double _Complex ma[iterations][segment_steps];
  double _Complex mb[iterations][segment_steps];
  double _Complex mc[iterations][segment_steps];
  double _Complex md[iterations][segment_steps];

  // pull back orbits
  int g = 255 * linear2srgb(0.5);
  memset(ppm, g, ppm_bytes);
  for (int n = 0; n < iterations; ++n)
  {
    for (int s = 0; s < segment_steps; ++s)
    {
      int s2 = (s + segment_steps - 1) % segment_steps;

      if (n > 0)
      {
        // pull back x
        double _Complex xn0[p_length - n];
        for (int l = 0; l < p_length - n; ++l)
        {
          int m = l + 1;
          // Wolf Jung's equation 22
          double _Complex z = csqrt(cproj
            ( (1 - y0[1][s])
            / (1 - x0[1][s])
            * (x0[m][s] - x0[1][s])
            / (x0[m][s] - y0[1][s])
            ));
          // choose sign by continuity
          if (cnorm(-z - x0[l][s2]) < cnorm(z - x0[l][s2]))
          {
            z = -z;
          }
          xn0[l] = z;
        }

        // pull back y
        double _Complex yn0[q_length - n];
        for (int l = 0; l < q_length - n; ++l)
        {
          int m = l + 1;
          // Wolf Jung's equation 22 divided by y (near infinity)
          double _Complex z = csqrt(cproj
            ( (1 - y0[1][s])
            / (1 - x0[1][s])
            * (1 - x0[1][s] / y0[m][s])
            / (1 - y0[1][s] / y0[m][s])
            ));
          // choose sign by continuity
          if (cnorm(-z - y0[l][s2]) < cnorm(z - y0[l][s2]))
          {
            z = -z;
          }
          yn0[l] = z;
        }

        // copy results into path arrays
        for (int l = 0; l < p_length - n; ++l)
        {
          x0[l][s] = xn0[l];
        }
        for (int l = 0; l < q_length - n; ++l)
        {
          y0[l][s] = yn0[l];
        }
      }

      // compute function to invert pull-back
      double _Complex a = x0[1][s];
      double _Complex b = y0[1][s];
      ma[n][s] = b * (1 - a);
      mb[n][s] = a * (b - 1);
      mc[n][s] = 1 - a;
      md[n][s] = b - 1;

      // render image (TODO: port to OpenCL to run on GPU)
      if ((! single_image_only) ||
        (n == single_image_n && s == single_image_s))
      {
        // accumulate statistics on used number of samples
        double samples_0 = 0;
        double samples_1 = 0;
        double samples_2 = 0;

        #pragma omp parallel for \
          schedule(dynamic) \
          reduction(+:samples_0) \
          reduction(+:samples_1) \
          reduction(+:samples_2)
        for (int j = 0; j < height; ++j)
        {
          for (int i = 0; i < width; ++i)
          {
            // accumulate statistics on pixel progress
            int  sum0 = 0;
            vec3 sum1 = black;
            vec3 sum2 = black;
            for (int k = 0; k < max_samples; ++k)
            {

              // equirectangular projection
              double v = ((j + uniform(k, j, i, 1)) / height - 0.5)
                * pi;
              double u = ((i + uniform(k, j, i, 2)) / width - 0.5)
                * pi * 2;
              double px = cos(v) * cos(u);
              double py = cos(v) * sin(u);
              double pz = sin(v);
              double _Complex z0 = (px + I * py) / (1 - pz);
              dcomplex z = { z0, pi * 2 / width / (1 - pz)  };

              // push forward
              for (int l = n; l >= 0; --l)
              {
                // inverse of Wolf Jung's equation 22
                // z = z * z;
                z = dc_sqr(z);
                // z = cproj(z);
                z = dc_proj(z);
                // z = (ma * z + mb) / (mc * z + md);
                z = dc_div
                  ( dc_addc(dc_cmul(ma[l][s], z), mb[l][s])
                  , dc_addc(dc_cmul(mc[l][s], z), md[l][s])
                  );
                // z = cproj(z);
                z = dc_proj(z);
              }

              // choose hemisphere
              double _Complex c, c0;
              dcomplex w;
              vec3 colour;
              if (cnorm(z.z) < 1)
              {
                colour = white;
                c = p;
                c0 = c0_p;
                w = dc_cmul(R[s], z);
              }
              else
              {
                colour = black;
                c = q;
                c0 = c0_q;
                w = dc_proj(dc_cdiv(R[s], z));
              }
              if (isnan(creal(w.z)) || isnan(cimag(w.z)) ||
                isnan(creal(w.dz)) || isnan(cimag(w.dz)))
              {
                continue;
              }

              // colour Julia set
              for (int l = n + 1; l < julia_iterations; ++l)
              {
                if (! (cnorm(w.z) < 100))
                {
                  break;
                }
                w = dc_addc(dc_sqr(w), c);
              }
              if (cnorm(w.z) > 100)
              {
                double de = cabs(w.z) * log(cabs(w.z)) / cabs(w.dz);
                colour = v3_mix(v3_sub(white, colour), grey, tanh(de));
              }
              else
              {
                vec3 hsv = { { (carg(w.z - c0) / pi + 1) / 2, 1, 1 } };
                if (isnan(hsv.v[0]) || isinf(hsv.v[0]))
                {
                  continue;
                }
                colour = v3_mix(colour, hsv2rgb(hsv), 0.5);
              }

              // compute statistiscs
              sum2 = v3_add(sum2, v3_mul(colour, colour));
              sum1 = v3_add(sum1, colour);
              sum0 += 1;
              if (min_samples <= sum0)
              {
                vec3 s = v3_muls(v3_sub(v3_smul(sum0, sum2),
                  v3_mul(sum1, sum1)), 1.0 / (sum0 * sum0));
                double stddev = sqrt((s.v[0] + s.v[1] + s.v[2]) / 3);
                // ~99 % chance that next sample will change pixel by
                // less than quantum, so finish already
                if (2.5 * stddev < sum0 / 256.0)
                {
                  break;
                }
              }

            }
            // output pixel
            if (sum0 > 0)
            {
              sum1 = v3_muls(sum1, 1.0 / sum0);
              size_t ix = ((size_t) j * width + i) * 3;
              ppm[ix + 0] = 255 * linear2srgb(sum1.v[0]);
              ppm[ix + 1] = 255 * linear2srgb(sum1.v[1]);
              ppm[ix + 2] = 255 * linear2srgb(sum1.v[2]);
            }
            // else keep value in image buffer from previous frame
            // and hope for the best...

            samples_0 += 1;
            samples_1 += sum0;
            samples_2 += sum0 * sum0;
          }
        }

        // output PPM image on stdout
        printf("P6\n%d %d\n255\n", width, height);
        fwrite(ppm, ppm_bytes, 1, stdout);
        fflush(stdout);

        // output image statistics on stderr
        double mean = samples_1 / samples_0;
        double stddev = sqrt((samples_0  * samples_2 -
          samples_1 * samples_1) / (samples_0 * samples_0));
        fprintf(stderr, "samples: %f +/- %f\n", mean, stddev);

        if (single_image_only)
        {
          return 0;
        }
      }
    }
  }
  free(ppm);
  return 0;
}
